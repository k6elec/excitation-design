function MSdef = MScreate(f0,fmax,varargin)
% MScreate creates a MSdef struct using the excitationdesign toolbox
% 
%      MSdef = MScreate(f0,fmax);
%      MSdef = MScreate(f0,fmax,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - f0      check: @(x) isscalar(x)&(~isnan(x))
%      minimum frequency of the multisine (lowpass mode) or frequency
%      difference  between two tones (bandpass mode)
% - fmax      check: @(x) isscalar(x)&(~isnan(x))
%      maximum frequency of the multisine (lowpass mode) or center
%      frequency of  the multisine (bandpass mode)
% 
% Parameter/Value pairs:
% - 'mode'     default: 'lowpass'  check: @(x)any(strcmpi(x,{'lowpass','bandpass'}))
%      mode of the function. When lowpass mode is set, the multisine
%      will be a  lowpass multisine, when bandpass is selected, it
%      will be a bandpass  multisine.
% - 'grid'     default: 'full'  check: @(x)any(strcmpi(x,{'full','lin','log','odd','even','log-odd','odd-odd','odd-even','even-even','random-odd','random','even-odd'}))
%      grid type of the multisine. See MSgridInit for information about
%      the  different types
% - 'groupsize'     default: 3  check: @isscalar
%      defines the group size X for random-odd multisines, 1 out of
%      every X  tones is left out.
% - 'numtones'     default: 21  check: @isscalar
%      number of tones in a bandpass multisine. This should be an odd
%      number to  have the fc bin as an excited bin.
% - 'bandwidth'     default: []  check: @isscalar
%      bandwidth of the multisine. If this is specified, the tones
%      will be  spread equally over the bandwidth around fc. If you
%      specify this  parameter, the value in f0 will be ignored (works
%      only in bandpass mode)
% - 'amplitude'     default: 1  check: @isscalar
%      amplitude of each tone in the multisine. If you want a freqency-dependent
%       amplitude, edit MSdef.ampl
% - 'rms'     default: []  check: @isscalar
%      the rms of the generated multisine. If you specify the rms,
%      the amplitude  is overruled.
% - 'Rout'     default: 0  check: @isscalar
%      output impedance of the multisine source. use inf to generate
%      a current  source
% - 'DC'     default: []  check: @isscalar
%      DC offset of the multisine
% 
% Outputs:
% - MSdef      Type: struct
%      Describes the multisine and can be used by the other functions
%      in the excitationdesign toolbox.
% 
% Example:
%   % create the multisine
%   MSdef = MScreate(1,100,'rms',0.01);
%   % add the node to which the multisine should be connected
%   MSdef.MSnode = 'ref';
%   % perform an ADS simulation with this multisine
%   spec = ADSsimulateMS('netlist',MSdef);
%   % calculate the BLA of the system
%   G = calculateSISO_BLA(spec);
% 
% Adam Cooman
% 
% Version info:
%  09-01-2015 version 1
%  05-03-2015 Added weird multisines of Piet. (even and odd-even)
%  25-04-2015 Added the random multisine and made sure that the last bin is always excited in random multisines
%  28-05-2015 Added the option for a DC offset
% 
% This documentation was generated with the generateFunctionHelp function at 28-May-2015

p = inputParser();
p.FunctionName = 'MScreate';
p.CaseSensitive=false;
try % partialMatching is for Matlab 2013 or somethin
p.PartialMatching = true;
end
% minimum frequency of the multisine (lowpass mode) or frequency difference
% between two tones (bandpass mode)
p.addRequired('f0',@(x) isscalar(x)&(~isnan(x)));
% maximum frequency of the multisine (lowpass mode) or center frequency of
% the multisine (bandpass mode)
p.addRequired('fmax',@(x) isscalar(x)&(~isnan(x)));
% mode of the function. When lowpass mode is set, the multisine will be a
% lowpass multisine, when bandpass is selected, it will be a bandpass
% multisine.
p.addParamValue('mode','lowpass',@(x)any(strcmpi(x,{'lowpass','bandpass'})))
% grid type of the multisine. See MSgridInit for information about the
% different types
p.addParamValue('grid','full',@(x)any(strcmpi(x,{'full','lin','log','odd','even','log-odd','odd-odd','odd-even','even-even','random-odd','random','even-odd'})));
% defines the group size X for random-odd multisines, 1 out of every X
% tones is left out.
p.addParamValue('groupsize',3,@isscalar)
% number of tones in a bandpass multisine. This should be an odd number to
% have the fc bin as an excited bin.
p.addParamValue('numtones',21,@isscalar)
% bandwidth of the multisine. If this is specified, the tones will be
% spread equally over the bandwidth around fc. If you specify this
% parameter, the value in f0 will be ignored (works only in bandpass mode)  
p.addParamValue('bandwidth',[],@isscalar);
% amplitude of each tone in the multisine. If you want a freqency-dependent
% amplitude, edit MSdef.ampl
p.addParamValue('amplitude',1,@isscalar)
% the rms of the generated multisine. If you specify the rms, the amplitude
% is overruled.
p.addParamValue('rms',[],@isscalar);
% output impedance of the multisine source. use inf to generate a current
% source
p.addParamValue('Rout',0,@isscalar);
% DC offset of the multisine
p.addParamValue('DC',0,@isscalar);
p.parse(f0,fmax,varargin{:});
args = p.Results;

% set f0, fmin and fmax first
switch args.mode
    case 'lowpass'
        MSdef.f0 = args.f0;
        MSdef.fmin = args.f0;
        MSdef.fmax = args.fmax;
    case 'bandpass'
        if ~isempty(args.bandwidth)
            args.fc = args.fmax;
            MSdef.fmin = args.fc-args.bandwidth/2;
            MSdef.fmax = args.fc+args.bandwidth/2;
            MSdef.f0 = args.bandwidth/(args.numtones-1);
        else
            MSdef.f0 = args.f0;
            args.fc = args.fmax;
            % check whether numtones is odd or not
            if mod(args.numtones,2)==0
                warning('the amount of tones is even. This makes that fc is not an excited frequency.')
                oddTones=false;
            else
                oddTones=true;
            end
            % calculate the BW of the multisine
            if oddTones
                MSdef.fmin = args.fc-(args.numtones-1)/2*args.f0;
                MSdef.fmax = args.fc+(args.numtones-1)/2*args.f0;
            else
                MSdef.fmin = args.fc-(args.numtones)/2*args.f0;
                MSdef.fmax = args.fc+(args.numtones)/2*args.f0;
            end
        end
    otherwise
        error('impossiburu!');
end

switch args.grid
    case {'random-odd','random'}
        MSdef = MSgridInit(MSdef,args.grid,args.groupsize);
        MSdef.grid = MSgrid(MSdef);
        if strcmpi(args.mode,'lowpass')
            % find the column that contains the first bin, the first bin should always be excited
            [onecol,~] = find(MSdef.grid==1);
            % put the column with the first bin in fist place
            MSdef.grid = [ MSdef.grid(onecol,:); MSdef.grid((1:args.groupsize)~=onecol,:)];
        end
        % throw away the last column
        MSdef.grid = MSdef.grid(1:end-1,:);
        % and put the remaining bins in a sorted vector
        MSdef.grid = sort(MSdef.grid(:)).';
        % and make sure the last bin is also excited
        if strcmpi(args.mode,'lowpass')
            if (MSdef.grid(end)+0.5)*MSdef.f0<MSdef.fmax
                MSdef.grid(end+1) = 2*floor((round(MSdef.fmax/MSdef.f0)-1)/2)+1;
            end
        end
    otherwise
        MSdef = MSgridInit(MSdef,args.grid);
        MSdef.grid = MSgrid(MSdef);
end
% the frequency axis is just the grid multiplied with the base frequency
MSdef.freq = MSdef.f0*MSdef.grid;

% get the amplitude of the tones
if ~isempty(args.rms)
    % RMStotal = sqrt( sum( RMSi^2 ) )
    % and RMS(A*sin(wt)) = A/sqrt(2)
    % so A should be sqrt(2)/sqrt(N)*RMStotal
    MSdef.ampl=sqrt(2)*args.rms/sqrt(length(MSdef.grid))*ones(size(MSdef.grid));
else
    MSdef.ampl=args.amplitude*ones(size(MSdef.grid));
end

% set the output impedance of the source
MSdef.Rout=args.Rout;

% give the multisine a random phase
MSdef.phase = 2*pi*rand(size(MSdef.grid));

% if a DC offset is given, add it to the struct
MSdef.DC = args.DC;

end

% @generateFunctionHelp
% @author Adam Cooman
% @version 09-01-2015 version 1
% @version 05-03-2015 Added weird multisines of Piet. (even and odd-even)
% @version 25-04-2015 Added the random multisine and made sure that the last bin is always excited in random multisines
% @version 28-05-2015 Added the option for a DC offset

% @tagline creates a MSdef struct using the excitationdesign toolbox

% @output1 Describes the multisine and can be used by the other functions in
% @output1 the excitationdesign toolbox.

% @outputType1 struct

% @example % create the multisine
% @example MSdef = MScreate(1,100,'rms',0.01);
% @example % add the node to which the multisine should be connected
% @example MSdef.MSnode = 'ref';
% @example % perform an ADS simulation with this multisine
% @example spec = ADSsimulateMS('netlist',MSdef);
% @example % calculate the BLA of the system
% @example G = calculateSISO_BLA(spec);

%% generateFunctionHelp: old help, backed up at 28-May-2015. leave this at the end of the function
% MScreate creates a MSdef struct using the excitationdesign toolbox
% 
%      MSdef = MScreate(f0,fmax);
%      MSdef = MScreate(f0,fmax,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - f0      check: @isscalar
%      minimum frequency of the multisine (lowpass mode) or frequency
%      difference  between two tones (bandpass mode)
% - fmax      check: @isscalar
%      maximum frequency of the multisine (lowpass mode) or center
%      frequency of  the multisine (bandpass mode)
% 
% Parameter/Value pairs:
% - 'mode'     default: 'lowpass'  check: @(x)any(strcmpi(x,{'lowpass','bandpass'}))
%      mode of the function. When lowpass mode is set, the multisine
%      will be a  lowpass multisine, when bandpass is selected, it
%      will be a bandpass  multisine.
% - 'grid'     default: 'full'  check: @(x)any(strcmpi(x,{'full','lin','log','odd','even','log-odd','odd-odd','odd-even','random-odd'}))
%      grid type of the multisine. See MSgridInit for information about
%      the  different types
% - 'groupsize'     default: 3  check: @isscalar
%      defines the group size X for random-odd multisines, 1 out of
%      every X  tones is left out.
% - 'numtones'     default: 21  check: @isscalar
%      number of tones in a bandpass multisine. This should be an odd
%      number to  have the fc bin as an excited bin.
% - 'bandwidth'     default: []  check: @isscalar
%      bandwidth of the multisine. If this is specified, the tones
%      will be  spread equally over the bandwidth around fc. If you
%      specify this  parameter, the value in f0 will be ignored (works
%      only in bandpass mode)
% - 'amplitude'     default: 1  check: @isscalar
%      amplitude of each tone in the multisine. If you want a freqency-dependent
%       amplitude, edit MSdef.ampl
% - 'rms'     default: []  check: @isscalar
%      the rms of the generated multisine. If you specify the rms,
%      the amplitude  is overruled.
% - 'Rout'     default: 0  check: @isscalar
%      output impedance of the multisine source. use inf to generate
%      a current  source
% 
% Outputs:
% - MSdef      Type: struct
%      Describes the multisine and can be used by the other functions
%      in the excitationdesign toolbox.
% 
% Example:
%   % create the multisine
%   MSdef = MScreate(1,100,'rms',0.01);
%   % add the node to which the multisine should be connected
%   MSdef.MSnode = 'ref';
%   % perform an ADS simulation with this multisine
%   spec = ADSsimulateMS('netlist',MSdef);
%   % calculate the BLA of the system
%   G = calculateSISO_BLA(spec);
% 
% Adam Cooman
% 
% Version info:
%  09-01-2015 version 1
%  05-03-2015 added weird multisines of piet (even and odd-even)
% 
% This documentation was generated with the generateFunctionHelp function at 05-Mar-2015
