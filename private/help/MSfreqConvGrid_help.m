%% MSfreqConvGrid
% Computes the frequency conversion grid for the response of a periodic time varying system to a multisine excitation.

%% Syntax
% <html>
% <p><tt>	convGrid = MSfreqConvGrid(grid, bin0, M) </tt></p>
% </html>

%% Inputs
% <html>
% <table>
% <tr><td>grid</td><td>frequency lines at which the input is excited</td></tr>
% <tr><td>bin0</td><td>frequency bin number of the periodic time frequency of the system</td></tr>
% <tr><td>M   </td><td>integer which determines the sidebands that the function considers. E.g. A down conversion mixer would use M=-1, If the full conversion matrix is required, then on needs to specify an array (e.g. M=[-3:3])</td></tr>
% </table>
% </html>

%% Output
% <html>
% <table>
% <tr><td>convGrid</td><td>frequency lines at the output of the system</td></tr>
% </table>
% </html>

%% Version History
% <html>
% <table>
% <tr><td>Gerd Vandersteen</td><td>6/2007</td><td>Initial implementation</td></tr>
% </table>
% <p><b>IMEC and ELEC VUB</b></p>
% </html>

