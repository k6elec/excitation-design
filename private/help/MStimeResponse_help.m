%% MStimeResponse
% This function computes the time response of the multisine at given time points t. 

%% Syntax
% <html>
% <p><tt>x = MStimeResponse(MSdef, t, fc)</tt></p>
% </html>

%% Inputs
% <html>
% <table>
% <tr><td> MSdef</td> 	
% <td><p>A structure which contains the information about the multisine. MSdef should at least contain the following fields</p>
% <table>
% <tr><td>freq</td><td>The matrix of frequencies.</td></tr>
% <tr><td>phase</td><td>The phases of the different sinewaves.</td></tr>
% <tr><td>ampl</td><td>The amplitudes of the different sinewaves.	</td></tr>		    		
% </table>
% </td>
% </tr>
% <tr><td>t</td><td>N x 1 vector which contains the time instances at which the response will be calculated</td></tr>
% <tr><td>fc</td><td>The carrier frequency of the signal. If the carrier frequency is ommited, the fc=0, which corresponds to a baseband signal. If fc =0, then x are real values, otherwise, x are complex values.</td></tr>		
% </table>
% </html>

%% Output
% <html><table>
% <tr><td>x</td><td>N x 1 vector which contains the multisine at the requested time instances</td></tr>
% </table></html>

%% Version History
% ELEC VUB
