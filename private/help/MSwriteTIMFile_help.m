%% MSwriteTIMFile
% This function creates a .tim file from the multsine provided in the structure MSdef

%% Syntax
% <html>
% <p><tt>MSwriteTIMFile(MSdef, filename)</tt></p>
% </html>

%% Inputs
% <html>
% <table>
% <tr><td>filename</td><td>String which contains the name of the generated file</td></tr>
% <tr><td>MSdef</td><td><p>Structure which describes the multisine. MSdef should contain the following fieds:</p>
% <p>Fields that are necessary to describe the multisine</p>
% <table>
% <tr><td>MSdef.grid </td><td>harmonic grid with excited bins in the multisine (DC=0, f0=1, 2*f0=2, ...)</td></tr>
% <tr><td>MSdef.ampl </td><td>amplifude vector of the sine components in the multisine</td></tr>
% <tr><td>MSdef.phase</td><td>phase vector of the multisine, in radians</td></tr>
% <tr><td>MSdef.f0   </td><td>frequency resolution of the multisine</td></tr>
% </table>
% <p>Fields that are specific to this function:</p>
% <table>
% <tr><td>MSdef.numberOfPeriods</td><td>number of periods of the multisine this number doens't have to be integer. if, for example, the number 3.5 is passed, 3 periods of the multisine will be exported and a half period is added to the beginning to let transients damp out.</td></tr>
% <tr><td>MSdef.fs</td><td>sample frequency of the transient simulation</td></tr>
% </table>
% </td></tr>
% </table>
% </html>

%% Output
% The function generates a .tim file in the current folder which can be used in ADS

%% Version History
% <html>
% <table>
% <tr><td>Adam Cooman</td><td>05/2013</td><td>Version 1.0 </td></tr>
% <tr><td>Adam Cooman</td><td>31/05/2013</td><td>Fixed a bug when the filename was shorter than 3 characters.  Added a default value for fs.</td></tr>
% </table>
% <p><b>ELEC, VUB</b></p>
% </html>


