%% MScalculatePeriod
% this function calculates one period of the multisine defined in MSdef

%% Syntax
% <html>
% <p><tt>[MS,Time] = MScalculatePeriod(MSdef)</tt></p>
% </html>

%% Input
% <html>
% <table>
% <tr><td> MSdef </td><td><p>Structure which describes the multisine. Needed fields in MSdef are</p>
% <table>
% <tr><td>MSdef.grid</td><td>The integers in this vector represent the excited frequency lines of the multisine </td></tr>
% </table>
% <p>Optional fields</p>
% <table>
% <tr><td>MSdef.f0</td><td>frequency resolution of the multisine. If this is not specified, a frequency resolution of 1Hz is used. This makes the time scale arbitrary. The amplitude is not influenced by the frequency resolution anyway.</td></tr>
% <tr><td>MSdef.fs</td><td>sample frequency of the time domain waveform. If this is not specified, the Nyquist frequency for the multisine will be chosen.</td></tr>
% </table>
% </td></tr>
% </table>
% </html>

%% Output
% <html>
% <table>
% <tr><td>MS  </td><td>vector which contains one period of the multisine</td></tr>
% <tr><td>Time</td><td>vector which contains the time axis</td></tr>
% </table>
% </html>

%% Example

% call the function
[MS,Time] = MScalculatePeriod(MSdef)
% plot one period of the multisine
plot(Time,MS)

%% Version History
% <html>
% <p><b>ELEC VUB</b></p>
% </html>
