% this file contains examples on how to use the different functions in the
% ExcitationDesign toolbox. 
%
%   ELEC VUB
%   

%% Test to write MS to Spectre
clc
clear all
close all

MSdef.fmin=1;                             % minimum excited frequency in the multisine
MSdef.fmax=20;                             % maximum excited frequency in the multisine
MSdef.f0 = 1;                               % f0 determines the frequency resolution
MSdef = MSgridInit(MSdef, 'random-odd', 3); % initiate a random-odd grid where 
                                            % one out of 3 excitation components is left out
MSdef.grid = MSgrid(MSdef);     % generate the excitation grid.
                                % the function returns a 3 x N matrix with random 
                                % permutations of the bins in groups of 3. 
MSdef.grid=MSdef.grid(1:2,:);   % we can leave random bins out by using only the
                                % first two elements of each vector.    
MSdef.grid=sort(MSdef.grid(:)); % reshape and sort MSdef.grid to a column vector to continue.
MSdef.freq = MSdef.f0*MSdef.grid;           % get the excited frequencies
MSdef.ampl = ones(size(MSdef.freq));        % set the amplitude of every component to 1
MSdef.phase = 2*pi*rand(size(MSdef.freq));  % give the multisine a random phase

MSdef.Rout = 0;                             % use an ideal voltage source
MSdef.fs=1000;
MScalculatePeriod(MSdef);

MSwriteSpectre(MSdef,'multisinesourceSpectreTest')

%% Test to write MS to ADS
clear all
close all

MSdef.fmin=100;                             % minimum excited frequency in the multisine
MSdef.fmax=200;                             % maximum excited frequency in the multisine
MSdef.f0 = 1;                               % f0 determines the frequency resolution
MSdef = MSgridInit(MSdef, 'full');          % initiate the full multisine        
MSdef.grid = MSgrid(MSdef);                 % generate the excitation grid
MSdef.freq = MSdef.f0*sort(MSdef.grid(:));  % get the excited frequencies
MSdef.ampl = ones(size(MSdef.freq));        % set the amplitude of every component to 1
MSdef.phase = 2*pi*rand(size(MSdef.freq));  % give the multisine a random phase

MSdef.name = 'Multisine';   
MSdef.Rout = 0;                             % use an ideal voltage source
MSdef.pos = 'test';                         % the positive node of the source is called 'pos'
MSdef.neg = 'test2';                        % the negative node of the source is called 'neg'

MSwriteADS(MSdef, 'Z:/Signals/MultisineSourceADS')


%% Test to write MS to a .tim file
clear all
close all

MSdef.fmin=1;                             % minimum excited frequency in the multisine
MSdef.fmax=200;                             % maximum excited frequency in the multisine
MSdef.f0 = 1;                               % f0 determines the frequency resolution
MSdef = MSgridInit(MSdef, 'odd');           % initiate an odd grid where 
MSdef.grid = MSgrid(MSdef);                 % generate the excitation grid
MSdef.freq = MSdef.f0*sort(MSdef.grid(:));  % get the excited frequencies
MSdef.ampl = ones(size(MSdef.freq));        % set the amplitude of every component to 1
MSdef.phase = 2*pi*rand(size(MSdef.freq));  % give the multisine a random phase

% the number of simulated periods has to be added
MSdef.numberOfPeriods = 3.2;
% The sample frequency of the transient simulation has to be added as well
MSdef.fs = 100*MSdef.fmax;

MSwriteTIMFile(MSdef, 'Multisine');

% show the time domain signal of the multisine
[MS,Time]=MScalculatePeriod(MSdef);

figure('name','multisine')
plot(Time,MS,'-+')
xlabel('Time [s]')
ylabel('Amplitude')

