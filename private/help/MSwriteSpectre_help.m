%% MSwriteSpectre
% this function writes a multisine into a spectre netlist. 

%% Syntax
% <html>
% <p><tt>MSwriteSpectre(MSdef, filename)</tt></p>
% </html>

%% Inputs
% <html>
% <table>
% <tr><td>filename</td><td>String which contains the name of the output file</td></tr>
% <tr><td>MSdef</td><td><p>Structure which contains the following fields</p>
% <table>
% <tr><td>MSdef.freq</td><td>1xN Frequency vector of the multisine</td></tr>
% <tr><td>MSdef.ampl</td><td>1xN Amplitude vector of the multisine</td></tr>
% <tr><td>MSdef.phase</td><td>1xN Phase vector of the multisine</td></tr>
% <tr><td>MSdef.Rout</td><td>the output resistance of the multisine source. this can be any real value, zero if a voltage source is wanted and Inf if a current source is required.</td></tr>
% </table>
% <p>Optional fields in MSdef are:</p>
% <table>
% <tr><td>MSdef.name</td><td>The name of the multisine source component. standard is 'MultiSineSource'</td></tr>
% </table>
% </td></tr>
% </table>
% </html>

%% Output
% The function creates a file which contains the multisine netlist

%% Notes
% The netlist contains the multisine source subcircuit. It can then later be included into another spectre netlist where the multisine source can be added to the circuit

%% Version history
% <html>
% <table>
% <tr><td>Gerd</td><td></td><td>Original version</td></tr>
% <tr><td>Adam</td><td>04/2013</td><td>documenting</td></tr>
% </table>
% <p><b>ELEC VUB</b></p>
% </html>

