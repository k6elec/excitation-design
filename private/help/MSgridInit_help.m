%% MSgridInit
% sets the properties if the grid for certain types of multisines. 

%% Syntax
% <html>
% <p><tt>
% MSdef = MSgridInit( MSdef, type)<br>
% MSdef = MSgridInit( MSdef, 'random-odd', blocksize )
% </tt></p>
% </html>

%% Input
% <html>
% <table>
% <tr><td>MSdef</td><td>A struct which will contain the description of the multisine. No fields are needed in MSdef to call this function.</td></tr>
% <tr><td>type </td><td><p>type of the multisine. options are</p>
% <table>
% <tr><td>'lin'</td><td>all frequency bins are excited</td></tr>
% <tr><td>'log'</td><td>frequencies on a quasi-log grid are excited</td></tr>
% <tr><td>'odd'</td><td>all odd frequency bins are excited</td></tr>
% <tr><td>'log-odd'</td><td>all odd frequency bins on a quasi-log grid are excited</td></tr>
% <tr><td>'odd-odd'</td><td>all lines at 4*k+1 are excited</td></tr>
% <tr><td>'random-odd'</td><td>odd frequency grid but some components are left out. you should specify the block size from which the lines are left out. standard=3</td></tr>
% <tr><td>'pseudo-periodic'</td><td>something exotic</td></tr>
% <tr><td>'pseudo-periodic random-odd'</td><td>something exotic too</td></tr>
% </table>
% </td></tr>
% <tr><td>blocksize</td><td>special option for random-odd multisines. blocksize sets the group out of which random bins are left out</td></tr>
% </table>
% </html>

%% Output
% <html>
% <p>The following fields are added to MSdef</p>
% <table>
% <tr><td>MSdef.info     </td><td>name of the multisine type</td></tr>
% <tr><td>MSdef.startIdx </td><td>first excited bin</td></tr>
% <tr><td>MSdef.stepIdx  </td><td>step between excited bins.</td></tr>
% <tr><td>MSdef.fratio   </td><td>used for quasi-log multisines</td></tr>
% <tr><td>MSdef.blockSize</td><td>used for random-odd multisines</td></tr>
% </table>
% </html>

%% Version History
% <html>
% <table>
% <tr><td>Gerd</td><td></td><td>Original Code</td></tr>
% <tr><td>Adam</td><td>05/2013</td><td>Added Documentation</td></tr>
% </table>
% <p><b>ELEC,VUB</b></p>
% </html>


