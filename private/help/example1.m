%% Write a Multisine to Spectre
% In this example we show how to generate a multisine in MATLAB and export it into a spectre netlist. This netlist can then be included into the cicuit netlist.

%% Set the frequency properties of the multisine
% We start by setting the frequency properties of the multisine. To do this, we have 3 fields in MSdef to set:
%	fmin	minimum excited frequency in the multisine
%	fmax	maximum excited frequency in the multisine
% 	f0		frequency resolution of the multisine
MSdef.fmin=1;                              
MSdef.fmax=20;                             
MSdef.f0 = 1;             

%% Generate the grid of excited frequencies
% As a second step, we call both MSgrid functions on our MSdef to generate the frequency grid.
% We want a random-odd grid, so we pass 'random-odd' to MSgridInit. We leave 1 bin out of every 3 bins.
% After MSgridInit, we can call MSgrid.
MSdef = MSgridInit(MSdef, 'random-odd', 3); 
MSdef.grid = MSgrid(MSdef);     
 
%% Finalise the frequency grid
% When a Random-odd multisine is requested, MSgrid returns a matrix which contains permutations of the frequency bins.
% Because we want to remove bins out of a group of 3 MSdef.grid now contains a 3 x N matrix.
% To leave one bin out and to obtain a correct frequency grid, we take only the first two elements of the matrix, make a column out of it and sort everything
MSdef.grid=MSdef.grid(1:2,:);   
MSdef.grid=sort(MSdef.grid(:)); 

%% Finalise the multisine
% Now that we have the frequency grid, we can finalise the design of the multisine.
% We get the frequencies of the multisine by multiplying the grid with the frequency resolution
% We set the amplitude of every bin to 1
% We set the phase of every excited bin to a random number between zero and 2*pi
MSdef.freq = MSdef.f0*MSdef.grid;
MSdef.ampl = ones(size(MSdef.freq));
MSdef.phase = 2*pi*rand(size(MSdef.freq));

%% Add the fields for MSwriteSpectre
% Before we can 
MSdef.Rout = 0;                             % use an ideal voltage source

%% Write the multisine to Spectre
MSwriteSpectre(MSdef,'multisinesourceSpectreTest')

%% Include the multisine netlist into the circtuit netlist
% TODO