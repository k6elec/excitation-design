function out = MSwriteADS(MSdef, filename)
% Writes a multisine into an ADS netlist. 
%   
%       MSwriteADS(MSdef, filename)
%       out = MSwriteADS(MSdef)
%
%   filename is the name of the output file
%   MSdef is a structure which contains the following fields
%       MSdef.Rout  the output resistance of the multisine source. this can
%                   be any real value, zero if a voltage source is wanted 
%                   and Inf if a current source is required.
%       MSdef.grid  Grid of the multisine, generated by the MSGrid function
%       MSdef.freq  Frequency vector of the multisine
%       MSdef.ampl  Amplitude vector of the multisine
%       MSdef.phase Phase vector of the multisine
%                       freq ampl and phase should have the same size
%   Optional fields in MSdef are:
%       MSdef.name  The name of the multisine subcircuit 
%                       standard is 'MultiSineSource'
%       MSdef.pos 	name of the positive node of the multisine source
%                       standard is 'pos'
%       MSdef.neg 	name of the negative node of the multisine source
%                       standard is 'neg'
%       MSdef.addSource boolean which indicated whether or not to add the
%                       multisine source itself to the netlist, or just put the subcircuit
%                       definition in there. default=1;
%       MSdef.DC    DC-offset of the multisine that should be added to the struct.
%
% Output:
%   if a filename is given to the function, the multisine is written to the
%   file. If you want the 
%
%   Version history
%       Original version: Gerd
%       Adam    04/2013:    Debugging + documenting
%       Adam 27/05/2013:    Added check for '-' in MSdef.name because it caused a syntax error in ADS
%       Adam 30/05/2013:    Fixed a bug where the standard values for the positive and negative node were not set right
%                           Improved amplitude reliability of the ideal voltage source
%                           Added checks on the filename  
%       Adam 02/09/2013:    Added possibility to have the string output
%       Adam 05/09/2013:    Added option to remove the actual multisine source
%       Adam 16/12/2013:    Changed the way finite output impedance is handled.
%                           The previous method scaled the amplitude of the multisine
%       Piet 13/04/2015:    Added the option to add a DC-offset to the
%                           multisine.
% ELEC VUB

if ~isfield(MSdef,'name')
    MSdef.name = 'MultiSineSource';
else
    % the name should not contain '-'. remove it
    dashes=strfind(MSdef.name,'-');
    MSdef.name(dashes)='_';
end

if ~isfield(MSdef,'addSource')    
    MSdef.addSource=1;
end

if ~isfield(MSdef,'pos')
    MSdef.pos = 'pos';
end

if ~isfield(MSdef,'neg')
    MSdef.neg = 'neg';
end

if ~isfield(MSdef,'freq')
    MSdef.freq = MSdef.f0*MSdef.grid;
end

if ~isfield(MSdef,'DC')
    MSdef.DC = 0;
end

% Checking the resistor value
if (imag(MSdef.Rout) ~= 0)
    % Rout must be purely real
    error('Complex value for output resistor in multisine source %s not allowed!', MSdef.name);
end

if length(MSdef.freq)>500
    warning('ADS has shown strange behaviour if the amount of tones is very big, expect weird stuff.');
end

% add some general info
out = [];
out = [out sprintf('; This file contains one multisine for use in ADS simulations.\r\n')];
out = [out sprintf('; Automatically generated by MSwriteADS.\r\n')];

% Initiate subcircuit
if MSdef.addSource
    out = [out sprintf('define %s_subckt (pos neg)\r\n', MSdef.name)];
else
    out = [out sprintf('define %s (pos neg)\r\n', MSdef.name)];
end

if MSdef.Rout==Inf
    % An ideal current source is wanted. 

    % Add current sources
    out = [out 'I_Source:Iin neg pos Type="I_nTone" '];
    for k=1:length(MSdef.freq)
        out = [out sprintf('Freq[%d]=%1.20e Hz ', k, MSdef.freq(k))];
    end;
    for k=1:length(MSdef.freq)
        out = [out sprintf('I[%d]=polar(%e,%1.20e) A ', k, MSdef.ampl(k), 180/pi*MSdef.phase(k))];
    end;
    out = [out sprintf('Idc=%e mA Iac=0 mA\r\n',MSdef.DC*1e3)];
    
else
    % Voltage multisine source with either zero or finite output impedance is wanted.
    % A voltage controlled voltage source with input impedance 1 and the 
    % required output impedance is placed after the current sources.

    % Add current sources
    % fprintf(fid, '  I_Source:Iin int_1 0 Type="I_nTone" ');
    out = [out '  I_Source:Iin N__666 0 Type="I_nTone" '];
    for k=1:length(MSdef.freq)
        out = [out sprintf('Freq[%d]=%1.20e Hz ', k, MSdef.freq(k))];
    end
    for k=1:length(MSdef.freq)
        out = [out sprintf('I[%d]=polar(%e,%1.20e) A ', k, MSdef.ampl(k), 180/pi*MSdef.phase(k))];
    end
    out = [out sprintf('Idc=%e mA Iac=0 mA\r\n',MSdef.DC*1e3)];
    
    % ADS needs the following line to be able to use VCVS
    out = [out sprintf('	#uselib "ckt" , "VCVS"\r\n')];
    % add the VCVS with an input resistance of 1 Ohm to transfer the current into a voltage
    out = [out sprintf('  VCVS:Vsrc N__666 0 neg pos G=1 T=0.0 nsec R1=1 Ohm R2=%e Ohm F=0.0 GHz \r\n',MSdef.Rout)];
end

% End subcircuit
if MSdef.addSource
    out = [out sprintf('end %s_subckt \r\n', MSdef.name)];
else
    out = [out sprintf('end %s \r\n', MSdef.name)];
end

% If the source itself is required, add it
if MSdef.addSource
    out = [out sprintf('%s_subckt:%s %s %s\r\n', MSdef.name, MSdef.name, MSdef.pos, MSdef.neg)];
end

% now print to the file
if exist('filename','var')
    % make or overwrite the new file with the specified filename.
    [path,name,~]=fileparts(filename);
    fid=fopen(fullfile(path,[name '.net']), 'w');
    if fid==-1
        error(['cannot create the file to write to. Specified path is: "' filename '"'])
    end
    fprintf(fid,out); 
    fclose(fid);
end
