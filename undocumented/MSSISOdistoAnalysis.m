function result = MSSISOdistoAnalysis(signals, TF)

IN.grids = MScomputeGrids(signals.(TF.in).wantedGrid);
IN.X = signals.(TF.in).X;
IN.wanted = MSspectrumSelection(IN.X, IN.grids.wanted);
OUT.grids = MScomputeGrids(signals.(TF.out).wantedGrid);
OUT.X=signals.(TF.out).X;
OUT.wanted = MSspectrumSelection(OUT.X, OUT.grids.wanted);
OUT.evenInbandDisto = MSspectrumSelection(OUT.X, OUT.grids.evenInbandDisto);
OUT.oddInbandDisto = MSspectrumSelection(OUT.X, OUT.grids.oddInbandDisto);

H = OUT.wanted./IN.wanted;

for k=1:length(TF.intern)
    INint.grids = MScomputeGrids(signals.(TF.intern{k}{1}).wantedGrid);
    INint.X = signals.(TF.intern{k}{1}).X;
    INint.wanted = MSspectrumSelection(INint.X, INint.grids.wanted);

    H1 = INint.wanted./IN.wanted;

    OUTint.grids = MScomputeGrids(signals.(TF.intern{k}{2}).wantedGrid);
    OUTint.X=signals.(TF.intern{k}{2}).X;
    OUTint.wanted = MSspectrumSelection(OUTint.X, OUTint.grids.wanted);
    OUTint.evenInbandDisto = MSspectrumSelection(OUTint.X, OUTint.grids.evenInbandDisto);
    OUTint.oddInbandDisto = MSspectrumSelection(OUTint.X, OUTint.grids.oddInbandDisto);
    H2 = OUTint.wanted ./ INint.wanted;
    %     H2_interp = interp1(OUTint.grids.wanted, H2, OUTint.grids.inbandDisto);
    H2_evenInterp = interp1(OUTint.grids.wanted, H2, OUTint.grids.evenInbandDisto);
    H2_oddInterp = interp1(OUTint.grids.wanted, H2, OUTint.grids.oddInbandDisto);
    H3 = OUT.wanted ./ OUTint.wanted;
    %     H3_interp = interp1(OUT.grids.wanted, H3, OUT.grids.inbandDisto);
%     if(1)
%         H3_evenInterp = OUT.evenInbandDisto ./ OUTint.evenInbandDisto;
%         H3_oddInterp = OUT.oddInbandDisto ./ OUTint.oddInbandDisto;
%     else
        H3_evenInterp = interp1(OUT.grids.wanted, H3, OUT.grids.evenInbandDisto);
        H3_oddInterp = interp1(OUT.grids.wanted, H3, OUT.grids.oddInbandDisto);
%     end

    %     OUTint.corrected = MSspectrumSelection(OUTint.X, OUTint.grids.inbandDisto) - ...
    %         H2_interp .* MSspectrumSelection(INint.X, INint.grids.inbandDisto);
    %     OUT.disto = H3_interp .* OUTint.corrected;
    OUTint.evenCorrected = MSspectrumSelection(OUTint.X, OUTint.grids.evenInbandDisto) ...
        + H2_evenInterp .* MSspectrumSelection(INint.X, INint.grids.evenInbandDisto);
    OUT.evenDisto = H3_evenInterp .* OUTint.evenCorrected;
    OUTint.oddCorrected = MSspectrumSelection(OUTint.X, OUTint.grids.oddInbandDisto) ...
        + H2_oddInterp .* MSspectrumSelection(INint.X, INint.grids.oddInbandDisto);
    OUT.oddDisto = H3_oddInterp .* OUTint.oddCorrected;

if (0)
    results{k}.wanted = OUT.wanted;
    results{k}.evenDisto = OUT.evenDisto;
    results{k}.oddDisto = OUT.oddDisto;
    results{k}.wantedGrid = OUT.grids.wanted;
    results{k}.evenInbandDistoGrid = OUT.grids.evenInbandDisto;
    results{k}.oddInbandDistoGrid = OUT.grids.oddInbandDisto;
    else
            results{k}.wanted = OUTint.wanted;
    results{k}.evenDisto = OUTint.evenCorrected;
    results{k}.oddDisto = OUTint.oddCorrected;
    results{k}.wantedGrid = OUTint.grids.wanted;
    results{k}.evenInbandDistoGrid = OUTint.grids.evenInbandDisto;
    results{k}.oddInbandDistoGrid = OUTint.grids.oddInbandDisto;
end

    figure(1)
    subplot(2,3,k);
    plot(results{k}.wantedGrid.*signals.(TF.out).df, 20*log10(abs(results{k}.wanted)),'x', ...
        results{k}.evenInbandDistoGrid.*signals.(TF.out).df, 20*log10(abs(results{k}.evenDisto)),'r+', ...
        results{k}.oddInbandDistoGrid.*signals.(TF.out).df, 20*log10(abs(results{k}.oddDisto)),'g+')
    title(sprintf('%s -> %s', TF.intern{k}{1}, TF.intern{k}{2}))
    xlabel('freq (Hz)')
    ylabel('dBV')
    
end
