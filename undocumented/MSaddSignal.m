function signals = MSaddSignal(signals, name, x, time, grid)

global defaultSignals_

if (nargin < 4)
    time = defaultSignals_.time;
end

if (nargin < 5)
    grid = defaultSignals_.grid;
end


T = time(end)-time(1);
N = length(time);
tmp = diff(time);
dT = mean(tmp);
stddT = std(dT);

if (stddT > N*1e-7*dT)
    error('Time samples are not equidistant in time')
end

signals . (name) . t0 = time(1);
signals . (name) . x = x;
signals . (name) . X = fft(x);
signals . (name) . dT = dT;
signals . (name) . df = 1/dT/N;
signals . (name) . wantedGrid = grid;

