function MSgridPlot(grid, MSdef)

subplot(2,1,1);
plot(grid.', ones(size(grid)).'*diag(1:MSdef.blockSize), 'x');
tmp=axis;
axis([tmp(1) tmp(2) tmp(3)-1 tmp(4)+1])
if (isfield(MSdef, 'info'))
    title(MSdef.info);
end

subplot(2,1,2);
semilogx(grid.', ones(size(grid)).'*diag(1:MSdef.blockSize), 'x');
axis([tmp(1) tmp(2) tmp(3)-1 tmp(4)+1])
