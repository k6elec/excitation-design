function out=MSspectrumSelection(spectrum, grid)

out = zeros(size(grid));
idx1 = find(grid >= 0);
out(idx1) = spectrum(grid(idx1)+1);
idx2 = find(grid < 0);
out(idx2) = conj(spectrum(abs(grid(idx2))+1));
