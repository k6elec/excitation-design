function [excSpec1, oddSpec, evenSpec, excSpec2] = MSspectrumPlot(spec, grid1, grid2)

if (nargin < 3)
    grid2 = [];
end

fullGrid = min([grid1, grid2]):max([grid1, grid2]);

idx=find(rem(fullGrid,2)~=0);oddGrid = setdiff(setdiff(fullGrid(idx), grid1), grid2);
idx=find(rem(fullGrid,2)==0);evenGrid = setdiff(setdiff(fullGrid(idx), grid1), grid2);
excSpec1 = MSspectrumSelection(spec, grid1);
excSpec2 = MSspectrumSelection(spec, grid2);
oddSpec = MSspectrumSelection(spec, oddGrid);
evenSpec = MSspectrumSelection(spec, evenGrid);

plot(grid1, 20*log10(abs(excSpec1)), 'k+', ...
    grid2, 20*log10(abs(excSpec2)), 'kx', ...
    oddGrid, 20*log10(abs(oddSpec)), 'rx', ...
    evenGrid, 20*log10(abs(evenSpec)), 'b+');
