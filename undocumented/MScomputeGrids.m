function grids = MScomputeGrids(grid)

grids.wanted = grid;
grids.wantedBand = min(abs(grid)):max(abs(grid));
grids.inbandDisto = setdiff(grids.wantedBand, grid);
idx=find(rem(grids.inbandDisto,2)~=0);
grids.oddInbandDisto = grids.inbandDisto(idx);
idx=find(rem(grids.inbandDisto,2)==0);
grids.evenInbandDisto = grids.inbandDisto(idx);
