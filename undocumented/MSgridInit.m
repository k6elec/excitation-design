function MSdef = MSgridInit(MSdef, type, varargin )
% this function sets the properties if the grid for certain types of
% multisines. The different options for the multisine are:
%
%   MSdef = MSgridInit( MSdef, type, varargin );
%
%   Where MSdef is a struct which will contain the description of the 
%   multisine. No fields are needed in MSdef to call this function.
%
%   'lin'           all frequency bins are excited
%   'log'           frequencies on a quasi-log grid are excited
%   'odd'           all odd frequency bins are excited
%   'even'          all even frequency bins are excited
%   'log-odd'       all odd frequency bins on a quasi-log grid are excited
%   'odd-odd'       all lines at 4*k+1 are excited
%   'odd-even'      all lines at 4*k-2 are excited
%   'even-odd'      all lines at 4*k-1 are excited
%   'even-even'     all lines at 4*k are excited
%   'random-odd'    odd frequency grid but some components are left out.
%                   you should specify the block size from which the lines
%                   are left out. standard=3
%   'random'        full multisine with frequency lines left out
%   and 2 more exotic multisines:
%   'pseudo-periodic', 'pseudo-periodic random-odd'
%
%   The following fields are added to MSdef:
%
%       MSdef.info          name of the multisine type
%       MSdef.startIdx      first excited bin
%   `   MSdef.stepIdx       step between excited bins.
%       MSdef.fratio        used for quasi-log multisines
%       MSdef.blockSize     used for random-odd multisines
%
% ELEC VUB, Code by Gerd Vandersteen, documented by Adam Cooman, 05/2013


MSdef.info = type;
switch lower(type)
    case {'lin','full'}
        MSdef.startIdx = 1;
        MSdef.stepIdx = 1;
        MSdef.fratio = 1;
        MSdef.blockSize = 1;
    case 'log'
        MSdef.startIdx = 1;
        MSdef.stepIdx = 1;
        MSdef.fratio = 1.2;
        MSdef.blockSize = 1;
    case 'odd'
        MSdef.startIdx = 1;
        MSdef.stepIdx = 2;
        MSdef.fratio = 1;
        MSdef.blockSize = 1;
    case 'even'
        MSdef.startIdx = 2;
        MSdef.stepIdx = 2;
        MSdef.fratio = 1;
        MSdef.blockSize = 1;
    case 'log-odd'
        MSdef.startIdx = 1;
        MSdef.stepIdx = 2;
        MSdef.fratio = 1.2;
        MSdef.blockSize = 1;
    case 'odd-odd'
        MSdef.startIdx = 1;
        MSdef.stepIdx = 4;
        MSdef.fratio = 1;
        MSdef.blockSize = 1;
    case 'odd-even'
        MSdef.startIdx = 2;
        MSdef.stepIdx = 4;
        MSdef.fratio = 1;
        MSdef.blockSize = 1;
    case 'even-even'
        MSdef.startIdx = 4;
        MSdef.stepIdx = 4;
        MSdef.fratio = 1;
        MSdef.blockSize = 1;
    case 'even-odd'
        MSdef.startIdx = 3;
        MSdef.stepIdx = 4;
        MSdef.fratio = 1;
        MSdef.blockSize = 1;
    case 'random-odd'
        MSdef.startIdx = 1;
        MSdef.stepIdx = 2;
        MSdef.fratio = 1;
        if ~isempty(varargin)
            MSdef.blockSize = varargin{1};
        else
            warning('no block size provided, using standard = 3')
            MSdef.blockSize = 3;
        end
    case 'random'
        MSdef.startIdx = 1;
        MSdef.stepIdx = 1;
        MSdef.fratio = 1;
        if ~isempty(varargin)
            MSdef.blockSize = varargin{1};
        else
            warning('no block size provided, using standard = 3')
            MSdef.blockSize = 3;
        end
    case 'pseudo-periodic'
        MSdef.startIdx = 1;
        MSdef.stepIdx = varargin{1};
        MSdef.fratio = 1;
        MSdef.blockSize = 1;
    case 'pseudo-periodic random-odd'
        MSdef.startIdx = 1;
        MSdef.stepIdx = 2*varargin{2};
        MSdef.fratio = 1;
        MSdef.blockSize = varargin{1};
    otherwise
        error('provided multisine does not exist. read the documentation')
end


