function [phase,MAX] = MSoptimiseCF(MSdef,M)
% this function generates crest-factor optimised phases for a multisine
%
%   phase = MSoptimiseCF(MSdef,N);
%
% where MSdef is the struct that represents the multisine
% M is the amount of phase realisations that should be retuned
%
% TODO: this function has a lot of potential improvements:
%   - other CF optimisation algorithms should be included
%   - the interface should be customisable
%   - ...
%
% Adam Cooman, ELEC VUB

% A is the number of random attempts made
A = ceil(M*1.2);

% generate a random phase
phase = 2*pi*rand(A,length(MSdef.grid));
MAX = zeros(A,1);

for aa=1:A
    MSdef.phase = phase(aa,:);
    period = MScalculatePeriod(MSdef);
    MAX(aa) = max(abs(period));
end

% sort the multisines according to crest factor
[~,order]=sort(MAX);
% and return the N best multisines
phase = phase(order(1:M),:);
MAX = MAX(order(1:M));

end