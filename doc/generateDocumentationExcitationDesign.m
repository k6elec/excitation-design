clear all
clc

options.format='html';
options.evalCode=false;
options.showCode=true;
options.outputDir='doc';

functions = {'MScalculatePeriod','MSfreqConvGrid','MSgrid','MSgridInit','MSNLcontrib','MStimeResponse','MSwriteADS','MSwriteSpectre','MSwriteTIMFile'};

helptoc = fopen(fullfile(options.outputDir,'helptoc.xml'),'w');
fprintf(helptoc,'<?xml version=''1.0'' encoding="utf-8"?>\r\n');
fprintf(helptoc,'<toc version="2.0">\r\n');
fprintf(helptoc,'  <tocitem target="EDTinfo.html">Excitation Design Toolbox\r\n');
fprintf(helptoc,'    <tocitem target="funclist.html" image="HelpIcon.FUNCTION">Functions\r\n');

funclist = fopen(fullfile(options.outputDir,'functlist.html'),'w');
for ii=1:length(functions)
    name = functions{ii};
    publish(['private\help\' name '_help.m'],options)
    movefile(['doc\' name '_help.html'],['doc\' name '.html']);
    fprintf(funclist,'<a href="%s.html">%s</a></br>\r\n',name,name);
    fprintf(helptoc,'      <tocitem target="%s.html">%s</tocitem>\r\n',name,name);
end
fclose(funclist);

fprintf(helptoc,'    </tocitem>\r\n');
fprintf(helptoc,'  </tocitem>\r\n');
fprintf(helptoc,'</toc>\r\n');

fclose(helptoc);

% remove the 'contents' section from the file

for ii=1:length(functions)
    name=functions{ii};
    fid=fopen(['doc\' name '.html']);
    data=textscan(fid,'%s',Inf,'Delimiter','\r\n');
    fclose(fid);
    data=data{1};
    % find the contents
    for jj=1:length(data)
        line = data{jj};
        [start,stop]=regexp(line,'<h2>Contents</h2><div>.+?(</div>)');
        if ~isempty(start)
            disp(line);
            disp(['found: "' line(start:stop) '"']);
            data{jj}=[line(1:start-1) line(stop+1:end)];
        end
    end
    % print the file back
    fid=fopen(['doc\' name '.html'],'w');
    for jj=1:length(data)
    	fprintf(fid,'%s\r\n',data{jj});
    end
    fclose(fid);
end

