function conversionGrid = MSfreqConvGrid(grid, bin0, M)
% Computes the frequency conversion grid for the response of a periodic
% time varying system to a multisine excitation.
%
% conversionGrid = MSfreqConvGrid(grid, bin0, M)
%
% The input is excited at the frequency lines specified in 'grid'.
% The variable bin0, represents the bin number of the periodic time
% frequency of the system.
% The variable M is an integer which determines the sidebands that one
% needs to be considered. E.g. A down conversion mixer would use M=-1, If
% the full conversion matrix is required, then on needs to specify an array
% (e.g. M=[-3:3])
%
% (c) IMEC 2007
% History:
% - Initial implementation in 6/2007 by Gerd Vandersteen

conversionGrid = kron(bin0*(M(:)), ones(size(grid))) + ...
    kron(ones(length(M),1), grid);
