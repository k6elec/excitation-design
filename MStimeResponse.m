function x = MStimeResponse(MSdef, t, fc)
% This function computes the time response of the multisine at given time
% points t. 
%
%   x = MStimeResponse(MSdef, t, fc)
%
% MSdef should at least contain the following fields:
% 	freq	The matrix of frequencies.
% 	phase   The phases of the different sinewaves.
% 	ampl    The amplitudes of the different sinewaves.
%
% t are the time instances at which the response will be calculated
%
% fc: The carrier frequency of the signal. If the carrier frequency is
% ommited, the fc=0, which corresponds to a baseband signal. If fc =0,
% then x are real values, otherwise, x are complex values.
%
% ELEC VUB

if (nargin < 3)
    fc = 0;
end

t=t(:);
x = zeros(size(MSdef.freq,1), length(t));

if (fc == 0)
    for k=1:size(x,1)
        x(k,:) = cos(kron(2*pi*MSdef.freq(k,:),t) ...
            + repmat(MSdef.phase(k,:),length(t),1)) ...
            * MSdef.ampl(k,:).';
    end
else
    for k=1:size(x,1)
        x(k,:) = ...
            (cos(kron(2*pi*(MSdef.freq(k,:)-fc),t) ...
            + repmat(MSdef.phase(k,:),length(t),1)) + ...
            + sqrt(-1) * sin(kron(2*pi*(MSdef.freq(k,:)-fc),t) ...
            + repmat(MSdef.phase(k,:),length(t),1)) ...
            ) * MSdef.ampl(k,:).';
    end
end
