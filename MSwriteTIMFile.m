function [timesig,time]=MSwriteTIMFile(MSdef, filename)
% This function creates a .tim file from the multsine provided in the structure MSdef
%
%   MSwriteTIMFile(MSdef, filename)
%   [timesig,time]=MSwriteTIMFile(MSdef, filename)
%
% Fields that are necessary to describe the multisine
%
% 	MSdef.grid  harmonic grid with excited bins in the multisine (DC=0, f0=1, 2*f0=2, ...)
%   MSdef.ampl  amplifude vector of the sine components in the multisine
%   MSdef.phase phase vector of the multisine, in radians
%   MSdef.f0    frequency resolution of the multisine
%
% Fields that are specific to this function: (you probably will have to add them before calling MSwriteTIMFile)
%
%   MSdef.numberOfPeriods   number of periods of the multisine
%               this number doens't have to be integer. if, for example,
%               the number 3.5 is passed, 3 periods of the multisine will
%               be exported and a half period is added to the beginning to
%               let transients damp out.
%   MSdef.fs    sample frequency of the transient simulation
%
% Output:
%   timesig     Vector which contains the full time domain signal that is
%               written to the tim file
%   time        Vector which contains the time points of the timesig.
%
% Adam Cooman    05/2013 Version 1.0 
% Adam Cooman 31/05/2013 Fixed a bug when the filename was shorter than 3 characters
%                        Added a standard value for fs.   
%

oneperiod = MScalculatePeriod(MSdef);

% check whether fs is present, otherwise, apply nyquist
if ~isfield(MSdef,'fs')
    MSdef.fs=2*MSdef.f0*max(MSdef.grid);
end

% repeat the period for the demanded number of times
timesig=repmat(oneperiod,floor(MSdef.numberOfPeriods),1);
clear onePeriod

% add the remaining bit to the beginning of the multisine.
if MSdef.numberOfPeriods~=floor(MSdef.numberOfPeriods)
    timesig=[timesig(end-floor(length(oneperiod)*(MSdef.numberOfPeriods-floor(MSdef.numberOfPeriods)))+1:end);timesig];
end

% generate the time vector
time = ((0:length(timesig)-1)')/MSdef.fs;

% cut off a possible .tim from the name
[path,filename,~]=fileparts(filename);
filename=fullfile(path,filename);

% finally, generate the .tim file itself
filename = [filename '.tim'];
fid = fopen(filename, 'w');
fprintf(fid, '%s\r\n', filename );
fprintf(fid, '%s\r\n', 'BEGIN  TIMEDATA');
fprintf(fid, '%s\r\n', '# T ( SEC  V  R xx )');
fprintf(fid, '%s\r\n', ' %   time      voltage ');
output=[time(:) timesig(:)];
fprintf(fid,'    %12.10e    %12.10e\r\n',output');
fprintf(fid, '%s\r\n',' END');
fclose(fid);
disp([filename ' ready'])

end