function [MS,Time] = MScalculatePeriod(MSdef,oversample,fs)
% this function calculates one period of the multisine defined in MSdef
%
%   [MS,Time] = MScalculatePeriod(MSdef)
%
% Needed fields in MSdef are
%
%   MSdef.grid  The integers in this vector represent the excited frequency 
%               lines of the multisine 
%
%
% Optional fields
%   MSdef.f0    frequency resolution of the multisine. If this is not
%               specified, a frequency resolution of 1Hz is used. This
%               makes the time scale arbitrary. The amplitude is not 
%               influenced by the frequency resolution anyway.

if ~isfield(MSdef,'f0')
    MSdef.f0=1;
end

if ~exist('oversample','var')
    oversample = 1;
end

if exist('fs','var')
    if rem(fs,MSdef.f0)~=0
        error('Sampling frequency should be a multiple of f0.')
    end
end

% MSdef.grid can become a matrix when randomodd is used. make a column from
% it and sort the frequency bins before moving on
exBins = sort(MSdef.grid(:))+1;

% build the multisine, starting from the grid numbers only
if nargin==3
    spec = zeros(fs/MSdef.f0,1); 
else
    spec = zeros(2*oversample*exBins(end),1);
end
spec(exBins) =  MSdef.ampl(:).*exp(1i*MSdef.phase(:));

% if there's a DC field present, add the DC offset as well
if isfield(MSdef,'DC')
    spec(1) = MSdef.DC;
end

% now calculate the period
MS = real(ifft(spec))*length(spec);

%figure
%plot(db(singleSidedSpectrum),'r+');
%hold on
%plot(db([singleSidedSpectrum;0;conj(flipud(singleSidedSpectrum(2:end)))]),'ko');
%plot(db(fft(MS)),'bs');

% build the corresponding time axis
Time = (0:length(MS)-1)'/(length(MS)*MSdef.f0);


end