%% Test to write MS to ADS
clear all
close all

MSdef.fmin=150;
MSdef.fmax=200;
MSdef.f0 = 1;
MSdef = MSgridInit(MSdef, 'random-odd', 3);
MSdef.name = 'RandomOdd';
MSdef.grid = MSgrid(MSdef);
MSdef.freq = MSdef.f0*MSdef.grid(1,:);
MSdef.ampl = ones(size(MSdef.freq));
MSdef.phase = 2*pi*rand(size(MSdef.freq));
MSdef.Rout = 0;
MSdef.pos = 'test';
MSdef.neg = 'test2';

MSwriteADS(MSdef, 'MSwriteSpectreTest')


%% Test to write MS to ADS
clear all
close all

MSdef.fmin=150;
MSdef.fmax=200;
MSdef.f0 = 1;
MSdef = MSgridInit(MSdef, 'random-odd', 3);
MSdef.name = 'RandomOdd';
MSdef.grid = MSgrid(MSdef);
MSdef.freq = MSdef.f0*MSdef.grid(1,:);
MSdef.ampl = ones(size(MSdef.freq));
MSdef.phase = 2*pi*rand(size(MSdef.freq));
MSdef.Rout = 0;
MSdef.pos = 'test';
MSdef.neg = 'test2';

MSwriteADS(MSdef, 'MSwriteSpectreTest')



%% Test to compute complex response of computed using MATLAB
clear all
close all

MSdef.fmin=150;
MSdef.fmax=200;
MSdef.f0 = 1;
MSdef = MSgridInit(MSdef, 'random-odd', 3);
MSdef.name = MSdef.info;
MSdef.grid = MSgrid(MSdef);
MSdef.freq = MSdef.f0*MSdef.grid(1,:);
MSdef.ampl = ones(size(MSdef.freq));
MSdef.phase = 2*pi*rand(size(MSdef.freq));
MSdef.Rout = 50;

t=[0:0.001:1];t=t(1:end-1);
x1=MStimeResponse(MSdef, t);
x2=MStimeResponse(MSdef, t, 100);
X1=fft(x1);
X2=fft(x2);
