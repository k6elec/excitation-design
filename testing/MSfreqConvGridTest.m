K=0.5;
fprintf('===============\n');
for M=2:15;
    for stepIdx=M

        for offset=0:M
            found = 1;
            for startIdx = 1:M-1

                if (1)
                    MSdef.fmin=K*500/2;
                    MSdef.fmax=(K+1)*500/2;
                else
                    MSdef.fmin=0;
                    MSdef.fmax=(M/2-1)*500;
                    %                MSdef.fmax=(M-1)/2*500;
                end
                MSdef.f0 = 1;
                MSdef.startIdx = startIdx;
                MSdef.stepIdx = stepIdx;
                MSdef.fratio = 1;
                MSdef.blockSize = 1;
                MSdef.name = 'MS1';
                MSdef.grid = MSgrid(MSdef);

                grid = MSdef.grid(1,:);
                bin0 = offset+round(500 / MSdef.stepIdx) * MSdef.stepIdx;

                conversionGrid = MSfreqConvGrid([-grid grid], bin0, [-M:M]);

                tmp=sort(conversionGrid(:));
                if min(diff(tmp)) == 0
                    found = 0;
                end
            end
            if (found)
                fprintf('M=%g stepIdx=%g offset=%g\n', M, stepIdx, offset)
            end
        end
    end
end
% Results
% MSdef.fmax=(M-1)/2*500; fmin = 0; only startIdx=1 & stepIdx = M
%  ==> only odd M
% M=3 stepIdx=3 startIdx=1 offset=0
% M=3 stepIdx=3 startIdx=1 offset=1
% M=3 stepIdx=3 startIdx=1 offset=3
% M=5 stepIdx=5 startIdx=1 offset=3
% M=7 stepIdx=7 startIdx=1 offset=5
% M=9 stepIdx=9 startIdx=1 offset=7
% M=11 stepIdx=11 startIdx=1 offset=9
% M=13 stepIdx=13 startIdx=1 offset=11
% M=15 stepIdx=15 startIdx=1 offset=13

% MSdef.fmax=(M/2-1)*500; only startIdx=1 & stepIdx = M
%  ==> also even M

% M=3 stepIdx=3 startIdx=1 offset=0
% M=3 stepIdx=3 startIdx=1 offset=1
% M=3 stepIdx=3 startIdx=1 offset=2
% M=3 stepIdx=3 startIdx=1 offset=3
% M=4 stepIdx=4 startIdx=1 offset=0
% M=4 stepIdx=4 startIdx=1 offset=1
% M=4 stepIdx=4 startIdx=1 offset=3
% M=4 stepIdx=4 startIdx=1 offset=4
% M=5 stepIdx=5 startIdx=1 offset=3
% M=5 stepIdx=5 startIdx=1 offset=4
% M=6 stepIdx=6 startIdx=1 offset=3
% M=6 stepIdx=6 startIdx=1 offset=5
% M=7 stepIdx=7 startIdx=1 offset=5
% M=7 stepIdx=7 startIdx=1 offset=6
% M=8 stepIdx=8 startIdx=1 offset=3
% M=8 stepIdx=8 startIdx=1 offset=7
% M=9 stepIdx=9 startIdx=1 offset=7
% M=9 stepIdx=9 startIdx=1 offset=8
% M=10 stepIdx=10 startIdx=1 offset=9
% M=11 stepIdx=11 startIdx=1 offset=9
% M=11 stepIdx=11 startIdx=1 offset=10
% M=12 stepIdx=12 startIdx=1 offset=5
% M=12 stepIdx=12 startIdx=1 offset=11
% M=13 stepIdx=13 startIdx=1 offset=11
% M=13 stepIdx=13 startIdx=1 offset=12
% M=14 stepIdx=14 startIdx=1 offset=13
% M=15 stepIdx=15 startIdx=1 offset=13
% M=15 stepIdx=15 startIdx=1 offset=14

% MSdef.fmax=(M/2-1)*500; fmin=0 for all startIdx=1:M-1 & stepIdx = M
%  ==> limited to M <= 4
% M=3 stepIdx=3 offset=0
% M=3 stepIdx=3 offset=1
% M=3 stepIdx=3 offset=2
% M=3 stepIdx=3 offset=3
% M=4 stepIdx=4 offset=1
% M=4 stepIdx=4 offset=3

% In a single band for different K and all startIdx=1:M-1 & stepIdx = M
%   MSdef.fmin=K*500;
%   MSdef.fmax=(K+1)*500;
% K=0
% M=3 stepIdx=3 offset=0
% M=3 stepIdx=3 offset=3
% M=4 stepIdx=4 offset=1
% M=4 stepIdx=4 offset=3
% M=5 stepIdx=5 offset=0
% M=5 stepIdx=5 offset=5
% M=6 stepIdx=6 offset=3
% M=6 stepIdx=6 offset=5
% M=7 stepIdx=7 offset=7
% M=8 stepIdx=8 offset=1
% M=8 stepIdx=8 offset=3
% M=8 stepIdx=8 offset=5
% M=8 stepIdx=8 offset=7
% M=9 stepIdx=9 offset=0
% M=9 stepIdx=9 offset=9
% M=10 stepIdx=10 offset=1
% M=10 stepIdx=10 offset=3
% M=10 stepIdx=10 offset=5
% M=10 stepIdx=10 offset=7
% M=10 stepIdx=10 offset=9
% M=11 stepIdx=11 offset=11
% M=12 stepIdx=12 offset=1
% M=12 stepIdx=12 offset=3
% M=12 stepIdx=12 offset=5
% M=12 stepIdx=12 offset=7
% M=12 stepIdx=12 offset=9
% M=12 stepIdx=12 offset=11
% M=13 stepIdx=13 offset=13
% M=14 stepIdx=14 offset=1
% M=14 stepIdx=14 offset=3
% M=14 stepIdx=14 offset=5
% M=14 stepIdx=14 offset=7
% M=14 stepIdx=14 offset=9
% M=14 stepIdx=14 offset=11
% M=14 stepIdx=14 offset=13
% M=15 stepIdx=15 offset=15

% K = 1
% M=3 stepIdx=3 offset=0
% M=3 stepIdx=3 offset=3
% M=5 stepIdx=5 offset=0
% M=5 stepIdx=5 offset=5
% M=7 stepIdx=7 offset=7
% M=9 stepIdx=9 offset=0
% M=9 stepIdx=9 offset=9
% M=11 stepIdx=11 offset=11
% M=13 stepIdx=13 offset=13
% M=15 stepIdx=15 offset=15

% K=pi
% M=3 stepIdx=3 offset=0
% M=3 stepIdx=3 offset=1
% M=3 stepIdx=3 offset=2
% M=3 stepIdx=3 offset=3
% M=5 stepIdx=5 offset=0
% M=5 stepIdx=5 offset=5
% M=7 stepIdx=7 offset=7
% M=9 stepIdx=9 offset=0
% M=9 stepIdx=9 offset=9
% M=11 stepIdx=11 offset=11
% M=13 stepIdx=13 offset=13
% M=15 stepIdx=15 offset=15

% K=0.5
% M=3 stepIdx=3 offset=0
% M=3 stepIdx=3 offset=3
% M=5 stepIdx=5 offset=0
% M=5 stepIdx=5 offset=5
% M=7 stepIdx=7 offset=7
% M=9 stepIdx=9 offset=0
% M=9 stepIdx=9 offset=9
% M=11 stepIdx=11 offset=11
% M=13 stepIdx=13 offset=13
% M=15 stepIdx=15 offset=15
%  ==> M must be odd
