% this is a demo on how to use the MScreate function. I create a multisine
% and simulate it in ADS. The multisine is applied to a simple resistor, so
% the result is far from spectacular

% create a single sine. Using the MScreate statement like this will create
% a full multisine with a period of 1MHz and that excites frequencies up to
% 1 GHz. The amplitude of each tone is 1V.

path(genpath('V:\Toolbox'))

MSdef = MScreate(1e6,1e9);

% add the node to the multisine
MSdef.MSnode='ref';

% generate a simple netlist, consisting of a single 50 Ohm resistor
netlist = {'R:res 0 ref R=50'};

% write the netlist to disk and simulate
writeTextFile(netlist,'temp');
spec =  ADSsimulateMS('temp',MSdef,'simulator','HB','numberOfRealisations',1);
delete('temp');

% plot the result
plot(spec.freq,db(squeeze(spec.ref(:,1,:))),'+')