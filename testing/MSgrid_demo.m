MSdef.fmin=1;
MSdef.fmax=200;
MSdef.f0 = 1;

plotNbr=1;

MSdef = MSgridInit(MSdef, 'lin');
grid = MSgrid(MSdef);
figure(plotNbr);plotNbr=plotNbr+1;
MSgridPlot(grid, MSdef);

MSdef = MSgridInit(MSdef, 'log');
figure(plotNbr);plotNbr=plotNbr+1;
MSgridPlot(MSgrid(MSdef), MSdef);

MSdef = MSgridInit(MSdef, 'odd');
figure(plotNbr);plotNbr=plotNbr+1;
MSgridPlot(MSgrid(MSdef), MSdef);

MSdef = MSgridInit(MSdef, 'log-odd');
figure(plotNbr);plotNbr=plotNbr+1;
MSgridPlot(MSgrid(MSdef), MSdef);

MSdef = MSgridInit(MSdef, 'odd-odd');
figure(plotNbr);plotNbr=plotNbr+1;
MSgridPlot(MSgrid(MSdef), MSdef);

MSdef = MSgridInit(MSdef, 'random-odd', 3);
figure(plotNbr);plotNbr=plotNbr+1;
MSgridPlot(MSgrid(MSdef), MSdef);

MSdef = MSgridInit(MSdef, 'pseudo-periodic', 5);
figure(plotNbr);plotNbr=plotNbr+1;
MSgridPlot(MSgrid(MSdef), MSdef);

MSdef = MSgridInit(MSdef, 'pseudo-periodic random-odd', 3, 5);
figure(plotNbr);plotNbr=plotNbr+1;
MSgridPlot(MSgrid(MSdef), MSdef);

%% Computation of the different nonlinear contributions...
grid=MSgrid(MSdef);
grid1=grid(1,:);
[grid2, grid3, grid4, grid5] = MSNLcontrib(grid1);

plot(grid1, ones(size(grid1)),'x', grid2, 2*ones(size(grid2)),'x', ...
    grid3, 3*ones(size(grid3)), 'x', grid4, 4*ones(size(grid4)), 'x', ...
    grid5, 5*ones(size(grid5)), 'x')
