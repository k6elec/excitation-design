% Set the output resistance of the source
% Rout = 0 for a voltage source
MSdef.Rout = 0;
% Change the names of the nodes the source is connected to
MSdef.pos = 'input';
MSdef.neg = 'ground';
% Call the MSwrite function
MSwriteADS(MSdef,'MultisineSourceADS')