% Set the output resistance of the source
% Rout = 0 for a voltage source
MSdef.Rout = 0;
% Call the MSwrite functions
MSwriteSpectre(MSdef,'MultisineSource')
MSwriteADS(MSdef,'Multisinesource')