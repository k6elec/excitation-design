MSdef.fmin=1e3;
MSdef.fmax=1e6;
MSdef.f0 = 1e3;
MSdef = MSgridInit(MSdef,'random-odd',4);
MSdef.grid = MSgrid(MSdef);
MSdef.freq = MSdef.f0*MSdef.grid(1,:);
MSdef.ampl = 1e-4*ones(size(MSdef.freq));
MSdef.phase = 2*pi*rand(size(MSdef.freq));