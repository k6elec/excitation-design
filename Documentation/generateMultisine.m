% minimum excited frequency in the multisine
MSdef.fmin=100;     
% maximum excited frequency in the multisine                        
MSdef.fmax=200;           
% f0 determines the frequency resolution                  
MSdef.f0 = 1;     
% initiate a random-odd grid where
% one out of 4 excitation components is left out                           
MSdef = MSgridInit(MSdef,'random-odd',4);
% generate the excitation grid                                            
MSdef.grid = MSgrid(MSdef);
MSdef.grid=MSdef.grid(1:end-1,:);
MSdef.grid=sort(MSdef.grid(:));
% from the grid find the excited frequencies                 
MSdef.freq = MSdef.f0*MSdef.grid;
% set the amplitude of every component to 1      
MSdef.ampl = ones(size(MSdef.freq));
% give the multisine a random phase        
MSdef.phase = 2*pi*rand(size(MSdef.freq));  