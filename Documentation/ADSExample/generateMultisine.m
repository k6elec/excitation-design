MSdef.f0 = 1e6;
MSdef.fmin=1e9-20*MSdef.f0;
MSdef.fmax=1e9+20*MSdef.f0;
MSdef = MSgridInit(MSdef,'full');
MSdef.grid = MSgrid(MSdef);
MSdef.freq = MSdef.f0*MSdef.grid(1,:);
MSdef.ampl = 0.001*ones(size(MSdef.freq));
MSdef.phase = 2*pi*rand(size(MSdef.freq));